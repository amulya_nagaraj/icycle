package android.primary.com.icycle.activity;

import android.content.Intent;
import android.os.Parcelable;
import android.primary.com.icycle.R;
import android.primary.com.icycle.UiUtils;
import android.primary.com.icycle.model.ApiErrorResponse;
import android.primary.com.icycle.model.CategoryData;
import android.primary.com.icycle.model.CreateUserResponse;
import android.primary.com.icycle.model.SendCodeRequest;
import android.primary.com.icycle.model.StringResponse;
import android.primary.com.icycle.model.UserDetail;
import android.primary.com.icycle.network.AwbApis;
import android.primary.com.icycle.network.AwbService;
import android.primary.com.icycle.utils.ApiConstants;
import android.primary.com.icycle.utils.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {

    private TextView btnScan, txtRedeem, txtRecycled, txtPurchased, txtRedeemValue;
    private int RESULT_SCAN = 100;
    private TextView txtName, txtLogout;
    private RelativeLayout toolRl, recycledRl, purchasedRl;
    private int totalRecycled, totalPurchased;
    private List<CategoryData> categoryDataList = new ArrayList<>();
    private boolean isFromScanned;
    private String successMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user2);
        initView();
        setData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserDetails();
    }

    private void callLogout() {
        UiUtils.showProgress(this);
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<StringResponse> call =
                apiService.logout(PreferenceManager.getString(this, ApiConstants.User.TOKEN));
        call.enqueue(new Callback<StringResponse>() {
            @Override
            public void onResponse(Call<StringResponse> call, @NonNull Response<StringResponse> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    PreferenceManager.clearPreferences(UserActivity.this);
                    Intent intent = new Intent(UserActivity.this, SplashActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(UserActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<StringResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(UserActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUserDetails() {
        UiUtils.showProgress(this);
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<UserDetail> call =
                apiService.getUserDetails(PreferenceManager.getString(this, ApiConstants.User.TOKEN));
        call.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, @NonNull Response<UserDetail> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    if (response.body() != null) {
                        setUserData(response.body());
                        if (isFromScanned) {
                            isFromScanned = false;
                            Toast.makeText(UserActivity.this, successMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(UserActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(UserActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData() {
        txtName.setText(getString(R.string.welcome_back) + " " + PreferenceManager.getString(this, ApiConstants.User.NAME));
    }

    private void setUserData(UserDetail userData) {
        txtRedeemValue.setText(String.valueOf(userData.getUserPoints()));
        txtRecycled.setText(String.valueOf(userData.getTotalCompletedCount()));
        txtPurchased.setText(String.valueOf(userData.getTotalActiveCount()));
        totalPurchased = userData.getTotalActiveCount();
        totalRecycled = userData.getTotalCompletedCount();
        categoryDataList = userData.getCategoryDataList();
        if (userData.getUserPoints() >= 50) {
            txtRedeem.setVisibility(View.VISIBLE);
            showRewardPointsDialog();
        }
    }

    private void initView() {
        btnScan = (TextView) findViewById(R.id.txt_scan);
        txtName = (TextView) findViewById(R.id.txt_name);
        toolRl = (RelativeLayout) findViewById(R.id.include_toolbar);
        txtLogout = (TextView) toolRl.findViewById(R.id.logout);
        txtRedeem = (TextView) findViewById(R.id.txt_redeem);
        txtRecycled = (TextView) findViewById(R.id.txt_recycled_amount);
        txtPurchased = (TextView) findViewById(R.id.txt_purchased_amount);
        txtRedeemValue = (TextView) findViewById(R.id.txt_reward);
        recycledRl = (RelativeLayout) findViewById(R.id.recycled_container);
        purchasedRl = (RelativeLayout) findViewById(R.id.purchased_container);
        txtRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRedeemDialog();
            }
        });
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserActivity.this, ScanActivity.class);
                startActivityForResult(intent, RESULT_SCAN);
            }
        });
        txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callLogout();
            }
        });
        recycledRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalRecycled > 0) {
                    Intent intent = new Intent(UserActivity.this, DetailsActivity.class);
                    intent.putExtra(ApiConstants.Bundle.IS_RECYCLED, true);
                    intent.putParcelableArrayListExtra(ApiConstants.User.CATEGORY_LIST, (ArrayList<? extends Parcelable>) categoryDataList);
                    startActivity(intent);
                } else {
                    Toast.makeText(UserActivity.this, "There are no recycled items yet", Toast.LENGTH_LONG).show();
                }
            }
        });
        purchasedRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalPurchased > 0) {
                    Intent intent = new Intent(UserActivity.this, DetailsActivity.class);
                    intent.putExtra(ApiConstants.Bundle.IS_RECYCLED, false);
                    intent.putParcelableArrayListExtra(ApiConstants.User.CATEGORY_LIST, (ArrayList<? extends Parcelable>) categoryDataList);
                    startActivity(intent);
                } else {
                    Toast.makeText(UserActivity.this, "There are no purchased items yet", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showRedeemDialog() {
        if (!isFinishing()) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_redeem_verification, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();

            TextView txtConfirm;
            txtConfirm = (TextView) dialogView.findViewById(R.id.txt_okay);

            txtConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    callRedeemCoupon();
                }
            });
            alertDialog.show();
        }

    }

    private void showRewardPointsDialog() {
        if (!isFinishing()) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_reward_points, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog alertDialog = dialogBuilder.create();

            TextView txtConfirm;
            txtConfirm = (TextView) dialogView.findViewById(R.id.txt_okay);

            txtConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }

    }

    private void callRedeemCoupon() {
        UiUtils.showProgress(this);
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<StringResponse> call =
                apiService.redeemPoint(PreferenceManager.getString(this, ApiConstants.User.TOKEN));
        call.enqueue(new Callback<StringResponse>() {
            @Override
            public void onResponse(Call<StringResponse> call, @NonNull Response<StringResponse> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    txtRedeemValue.setText("0");
                    txtRedeem.setVisibility(View.GONE);
                    Toast.makeText(UserActivity.this, response.body().getDetail(), Toast.LENGTH_SHORT).show();
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(UserActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<StringResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(UserActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_SCAN && resultCode == RESULT_OK) {
            if (data != null) {
                String code = data.getStringExtra(ApiConstants.Bundle.SCAN_RESULT);
                handleScanCode(code);
            }
        }
    }

    private void handleScanCode(String code) {
        if (!TextUtils.isEmpty(code)) {
            SendCodeRequest sendCodeReq = new SendCodeRequest();
            sendCodeReq.setProductId(code);
            sendProductCode(sendCodeReq);
        } else {
            Toast.makeText(UserActivity.this, "Invalid code", Toast.LENGTH_LONG).show();
        }
    }

    private void sendProductCode(SendCodeRequest sendCodeReq) {
        UiUtils.showProgress(this);
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<StringResponse> call =
                apiService.sendProductId(PreferenceManager.getString(this, ApiConstants.User.TOKEN), sendCodeReq);
        call.enqueue(new Callback<StringResponse>() {
            @Override
            public void onResponse(Call<StringResponse> call, @NonNull Response<StringResponse> response) {

                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    isFromScanned = true;
                    if (response.body() != null) {
                        successMessage = response.body().getDetail();
                    }

                    getUserDetails();
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    UiUtils.hideProgress();
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(UserActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<StringResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(UserActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
