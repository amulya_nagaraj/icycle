package android.primary.com.icycle.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.primary.com.icycle.R;
import android.view.View;
import android.widget.TextView;

public class OnboardingActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtLogin, txtSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        initView();
        setListeners();
    }

    private void setListeners() {
        txtLogin.setOnClickListener(this);
        txtSignup.setOnClickListener(this);
    }

    private void initView() {
        txtLogin = (TextView) findViewById(R.id.txt_login);
        txtSignup = (TextView) findViewById(R.id.txt_signup);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.txt_login:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;

            case R.id.txt_signup:
                Intent signupIntent = new Intent(this, SignUpActivity.class);
                startActivity(signupIntent);
                break;
        }
    }
}
