package android.primary.com.icycle.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by amulya on 1/7/17.
 */

public class CategoryData implements Parcelable {

    private int activeCount;
    private int id;

    protected CategoryData(Parcel in) {
        activeCount = in.readInt();
        id = in.readInt();
        completedCount = in.readInt();
    }

    public static final Creator<CategoryData> CREATOR = new Creator<CategoryData>() {
        @Override
        public CategoryData createFromParcel(Parcel in) {
            return new CategoryData(in);
        }

        @Override
        public CategoryData[] newArray(int size) {
            return new CategoryData[size];
        }
    };

    public int getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(int activeCount) {
        this.activeCount = activeCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompletedCount() {
        return completedCount;
    }

    public void setCompletedCount(int completedCount) {
        this.completedCount = completedCount;
    }

    private int completedCount;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(activeCount);
        parcel.writeInt(id);
        parcel.writeInt(completedCount);
    }
}
