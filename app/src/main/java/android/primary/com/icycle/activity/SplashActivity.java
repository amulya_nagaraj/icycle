package android.primary.com.icycle.activity;

import android.content.Intent;
import android.primary.com.icycle.R;
import android.primary.com.icycle.utils.ApiConstants;
import android.primary.com.icycle.utils.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setTimer();
    }

    private void setTimer() {
        Timer timerObj = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(PreferenceManager.getString(SplashActivity.this, ApiConstants.User.TOKEN))) {
                    Intent intent = new Intent(SplashActivity.this, UserActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, OnboardingActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        };
        timerObj.schedule(timerTask, 2000);
    }
}
