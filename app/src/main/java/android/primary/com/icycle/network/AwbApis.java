/*
 * Copyright 2016 Y Media Labs. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package android.primary.com.icycle.network;

import android.primary.com.icycle.model.CreateUserRequest;
import android.primary.com.icycle.model.CreateUserResponse;
import android.primary.com.icycle.model.LoginUserRequest;
import android.primary.com.icycle.model.SendCodeRequest;
import android.primary.com.icycle.model.StringResponse;
import android.primary.com.icycle.model.UserDetail;
import android.support.annotation.NonNull;


import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by manjunath on 9/30/16.
 */

public interface AwbApis {

    @NonNull
    @POST("/api/auth/register/")
    Call<CreateUserResponse> createUser(@Body CreateUserRequest createUserRequest);

    @NonNull
    @POST("/api/auth/login/")
    Call<CreateUserResponse> loginUser(@Body LoginUserRequest loginUserRequest);

    @NonNull
    @GET("api/user-products/dashboard/")
    Call<UserDetail> getUserDetails(@Header("sessionId") String token);

    @NonNull
    @DELETE("/api/auth/logout/")
    Call<StringResponse> logout(@Header("sessionId") String token);

    @NonNull
    @POST("/api/user-products/assign/")
    Call<StringResponse> sendProductId(@Header("sessionId") String token, @Body SendCodeRequest sendCodeRequest);

    @NonNull
    @GET("api/user-products/redeem-coupon/")
    Call<StringResponse> redeemPoint(@Header("sessionId") String token);


}
