package android.primary.com.icycle.model;

/**
 * Created by amulya on 1/7/17.
 */

public class CreateUserResponse {

    private String token;
    private CreateUserRequest user;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public CreateUserRequest getUser() {
        return user;
    }

    public void setUser(CreateUserRequest user) {
        this.user = user;
    }


}
