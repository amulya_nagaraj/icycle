package android.primary.com.icycle.activity;

import android.content.Intent;
import android.primary.com.icycle.UiUtils;
import android.primary.com.icycle.model.ApiErrorResponse;
import android.primary.com.icycle.model.CreateUserRequest;
import android.primary.com.icycle.model.CreateUserResponse;
import android.primary.com.icycle.model.LoginUserRequest;
import android.primary.com.icycle.network.AwbApis;
import android.primary.com.icycle.network.AwbService;
import android.primary.com.icycle.utils.ApiConstants;
import android.primary.com.icycle.utils.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.primary.com.icycle.R;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private EditText edtEmail, edtPwd;
    private TextView txtNext;
    private ImageView imgEye;
    private boolean isPwdVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }


    private void initView() {
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPwd = (EditText) findViewById(R.id.edt_password);
        txtNext = (TextView) findViewById(R.id.txt_next);
        imgEye = (ImageView) findViewById(R.id.img_eye);
        imgEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPwdVisible) {
                    isPwdVisible = false;
                    edtPwd.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    isPwdVisible = true;
                    edtPwd.setTransformationMethod(null);
                }
                edtPwd.setSelection(edtPwd.getText().length());

            }
        });
        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UiUtils.hideKeyboard(view);
                validateFields();
            }
        });
        edtEmail.requestFocus();
        UiUtils.showKeyboard(edtEmail);
    }

    private void validateFields() {
        String email = edtEmail.getText().toString().trim();
        String password = edtPwd.getText().toString().trim();
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            if (UiUtils.isValidEmail(email)) {
                LoginUserRequest loginUserRequest = new LoginUserRequest();
                loginUserRequest.setPassword(password);
                loginUserRequest.setEmail(email);
                UiUtils.showProgress(this);
                loginUser(loginUserRequest);
            } else {
                Toast.makeText(this, "Please input a valid email", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.enter_fields), Toast.LENGTH_LONG).show();
        }

    }

    private void loginUser(LoginUserRequest loginUserRequest) {
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<CreateUserResponse> call =
                apiService.loginUser(loginUserRequest);
        call.enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, @NonNull Response<CreateUserResponse> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    if (response != null && response.body() != null) {
                        PreferenceManager.set(LoginActivity.this, ApiConstants.User.TOKEN, response.body().getToken());
                        if (response.body().getUser() != null)
                            PreferenceManager.set(LoginActivity.this, ApiConstants.User.NAME, response.body().getUser().getFirstName());
                        Intent intent = new Intent(LoginActivity.this, UserActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(LoginActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CreateUserResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(LoginActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
