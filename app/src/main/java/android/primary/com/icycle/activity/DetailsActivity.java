package android.primary.com.icycle.activity;

import android.primary.com.icycle.model.CategoryData;
import android.primary.com.icycle.utils.ApiConstants;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.primary.com.icycle.R;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {
    private boolean isFromRecycled;
    private TextView txtTitle, txtPlastic, txtCard, txtCans;
    private List<CategoryData> categoryDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        if (getIntent() != null) {
            isFromRecycled = getIntent().getBooleanExtra(ApiConstants.Bundle.IS_RECYCLED, false);
            categoryDataList = getIntent().getParcelableArrayListExtra(ApiConstants.User.CATEGORY_LIST);
        }
        initView();
        setData();
    }

    private void initView() {
        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtPlastic = (TextView) findViewById(R.id.txt_plastic);
        txtCard = (TextView) findViewById(R.id.txt_card);
        txtCans = (TextView) findViewById(R.id.txt_cans_amount);
    }

    private void setData() {
        if (isFromRecycled) {
            txtTitle.setText(getString(R.string.product_recycled));
            if (categoryDataList.size() > 0) {
                txtPlastic.setText(String.valueOf(categoryDataList.get(0).getCompletedCount()));
                txtCard.setText(String.valueOf(categoryDataList.get(1).getCompletedCount()));
                txtCans.setText(String.valueOf(categoryDataList.get(2).getCompletedCount()));
            }
        } else {
            txtTitle.setText(getString(R.string.product_purchased));
            if (categoryDataList.size() > 0) {
                txtPlastic.setText(String.valueOf(categoryDataList.get(0).getActiveCount()));
                txtCard.setText(String.valueOf(categoryDataList.get(1).getActiveCount()));
                txtCans.setText(String.valueOf(categoryDataList.get(2).getActiveCount()));
            }
        }
    }
}
