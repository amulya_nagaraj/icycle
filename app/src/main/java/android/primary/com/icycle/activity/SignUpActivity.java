package android.primary.com.icycle.activity;

import android.content.Intent;
import android.primary.com.icycle.UiUtils;
import android.primary.com.icycle.model.ApiErrorResponse;
import android.primary.com.icycle.model.CreateUserRequest;
import android.primary.com.icycle.model.CreateUserResponse;
import android.primary.com.icycle.network.AwbApis;
import android.primary.com.icycle.network.AwbService;
import android.primary.com.icycle.utils.ApiConstants;
import android.primary.com.icycle.utils.PreferenceManager;
import android.primary.com.icycle.R;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    private TextView txtNext;
    private EditText edtNum, edtName, edtEmail, edtPwd;
    private boolean isPwdVisible;
    private ImageView imgEye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        initView();
    }

    private void initView() {
        txtNext = (TextView) findViewById(R.id.txt_next);
        edtNum = (EditText) findViewById(R.id.edt_number);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPwd = (EditText) findViewById(R.id.edt_password);
        imgEye = (ImageView) findViewById(R.id.img_eye);
        txtNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UiUtils.hideKeyboard(view);
                validateFields();
            }
        });
        imgEye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPwdVisible) {
                    isPwdVisible = false;
                    edtPwd.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    isPwdVisible = true;
                    edtPwd.setTransformationMethod(null);
                }
                edtPwd.setSelection(edtPwd.getText().length());

            }
        });
        edtName.requestFocus();
        UiUtils.showKeyboard(edtName);
    }

    private void validateFields() {
        String number = edtNum.getText().toString().trim();
        String name = edtName.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();
        String password = edtPwd.getText().toString().trim();
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(number) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            if (UiUtils.isValidEmail(email)) {
                if (number.length() == 10) {
                    PreferenceManager.set(SignUpActivity.this, ApiConstants.User.PHONE_NUM, number);
                    PreferenceManager.set(SignUpActivity.this, ApiConstants.User.NAME, name);
                    CreateUserRequest createUserRequest = new CreateUserRequest();
                    createUserRequest.setFirstName(name);
                    createUserRequest.setLastName(name);
                    createUserRequest.setPhoneNumber(number);
                    createUserRequest.setPassword(password);
                    createUserRequest.setEmail(email);
                    UiUtils.showProgress(SignUpActivity.this);
                    registerUser(createUserRequest);
                } else {
                    Toast.makeText(SignUpActivity.this, "Please input a valid number", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(SignUpActivity.this, "Please input a valid email", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(SignUpActivity.this, getString(R.string.enter_fields), Toast.LENGTH_LONG).show();
        }

    }

    private void registerUser(CreateUserRequest createUserRequest) {
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<CreateUserResponse> call =
                apiService.createUser(createUserRequest);
        call.enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, @NonNull Response<CreateUserResponse> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    if (response != null && response.body() != null) {
                        PreferenceManager.set(SignUpActivity.this, ApiConstants.User.TOKEN, response.body().getToken());
                        Intent intent = new Intent(SignUpActivity.this, UserActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(SignUpActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CreateUserResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(SignUpActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
