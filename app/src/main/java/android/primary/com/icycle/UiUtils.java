package android.primary.com.icycle;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by amulya on 1/7/17.
 */

public class UiUtils {
    @Nullable
    private static ProgressDialog sProgressDialog;
    public static final String TAG = UiUtils.class.getSimpleName();


    public static final boolean isValidEmail(@Nullable CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * To show progress and block the user
     *
     * @param activity : activity
     */
    public static void showProgress(@Nullable Activity activity) {

        if (null != activity) {
            if (null == sProgressDialog) {
                sProgressDialog = new ProgressDialog(activity);
                sProgressDialog.setProgressStyle(R.style.MolekuleProgressTheme);
            }
            sProgressDialog.setIndeterminate(true);
            sProgressDialog.setCancelable(false);
            if (sProgressDialog.getWindow() != null) {
                sProgressDialog.getWindow()
                        .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }

            try {
                if (!sProgressDialog.isShowing())
                    sProgressDialog.show();
            } catch (WindowManager.BadTokenException e) {
                Log.d(TAG, "WindowManager.BadTokenException:" + e);
            }
            sProgressDialog.setContentView(R.layout.layout_progress_dialog);

        }
    }

    /**
     * To dismiss the progress
     */
    public static void hideProgress() {
        if (null != sProgressDialog) {
            sProgressDialog.dismiss();

            sProgressDialog = null;
        }
    }

    /**
     * This method hides the soft keyboard.
     *
     * @param view view of the calling method
     */
    public static void hideKeyboard(@Nullable View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * @param view Show keyboard when view is in focus
     */
    public static void showKeyboard(@Nullable View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);

        }
    }
}
