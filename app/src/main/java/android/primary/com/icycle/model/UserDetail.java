package android.primary.com.icycle.model;

import java.util.List;

/**
 * Created by amulya on 1/7/17.
 */

public class UserDetail {
    private int totalActiveCount;
    private int userPoints;
    private int totalCompletedCount;

    public int getTotalActiveCount() {
        return totalActiveCount;
    }

    public void setTotalActiveCount(int totalActiveCount) {
        this.totalActiveCount = totalActiveCount;
    }

    public int getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }

    public int getTotalCompletedCount() {
        return totalCompletedCount;
    }

    public void setTotalCompletedCount(int totalCompletedCount) {
        this.totalCompletedCount = totalCompletedCount;
    }

    public List<CategoryData> getCategoryDataList() {
        return categories;
    }

    public void setCategoryDataList(List<CategoryData> categoryDataList) {
        this.categories = categoryDataList;
    }

    private List<CategoryData> categories;
}
