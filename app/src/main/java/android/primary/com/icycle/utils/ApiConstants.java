package android.primary.com.icycle.utils;

import android.primary.com.icycle.ICycleApplication;
import android.primary.com.icycle.R;

/**
 * Created by amulya on 30/6/17.
 */

public class ApiConstants {
    public static final String BASE_URL = "http://ec2-18-220-53-253.us-east-2.compute.amazonaws.com:8000";
//    public static final String BASE_URL = "http://172.16.2.183:8000";
    public static final String EMPTY = "";
    public static final int SUCCESS_200 = 200;
    public static final int ERROR_400 = 400;

    public interface User {
        String PHONE_NUM = "phone_number";
        String NAME = "name";
        String TOKEN = "token";
        String CATEGORY_LIST = "category_list";
    }

    public interface Bundle {
        String SCAN_RESULT = "scan_result";
        String IS_RECYCLED = "is_recycled";
    }

    public interface RECYCLABLE_ITEM_TYPE {
        int Plastic = 1;
        int Cardboard = 2;
        int Cans = 3;
    }

}
